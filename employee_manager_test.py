import datetime

from relations_manager import RelationsManager
from employee_manager import EmployeeManager
from employee import Employee

def test_not_leader_salary():
    rm = RelationsManager()
    em = EmployeeManager(relations_manager=rm)
    employee = Employee(
        id = 42,
        first_name = "test",
        last_name = "test",
        birth_date = datetime.date(1980,1,1),
        base_salary = 1000,
        hire_date = datetime.date(1998,10,10)
    )
    assert em.calculate_salary(employee) == 3000

def test_leader_salary():
    rm = RelationsManager()
    em = EmployeeManager(relations_manager=rm)
    employee = Employee(
        id = 42,
        first_name = "test",
        last_name = "test",
        birth_date = datetime.date(1980,1,1),
        base_salary = 2000,
        hire_date = datetime.date(2008,10,10)
    )

    rm.teams[42] = [1,2,3]
    assert em.calculate_salary(employee) == 3600

def test_email():
    rm = RelationsManager()
    em = EmployeeManager(relations_manager=rm)
    employee = Employee(
        id = 42,
        first_name = "test",
        last_name = "test",
        birth_date = datetime.date(1980,1,1),
        base_salary = 2000,
        hire_date = datetime.date(2008,10,10)
    )
    expected = f"{employee.first_name} {employee.last_name} your salary: {em.calculate_salary(employee)} has been transferred to you."
    message = em.calculate_salary_and_send_email(employee)
    assert expected == message
