import datetime

from relations_manager import RelationsManager

def test_john_doe_birthday():
    rm = RelationsManager()
    emp = rm.get_all_employees()
    john_doe = list(
        filter(
            lambda e: e.first_name == "John" and e.last_name == "Doe",
            emp
        )
    )[0]
    assert rm.is_leader(john_doe) == True
    assert john_doe.birth_date == datetime.date(1970,1,31)

def test_john_doe_s_team():
    rm = RelationsManager()
    emp = rm.get_all_employees()
    john_doe = list(
        filter(
            lambda e: e.first_name == "John" and e.last_name == "Doe",
            emp
        )
    )[0]
    assert rm.get_team_members(john_doe) == [2,3]

def test_john_doe_s_team_no_tomas():
    rm = RelationsManager()
    emp = rm.get_all_employees()
    john_doe = list(
        filter(
            lambda e: e.first_name == "John" and e.last_name == "Doe",
            emp
        )
    )[0]
    assert 5 not in rm.get_team_members(john_doe) 

def test_walford_base_salary():
    rm = RelationsManager()
    emp = rm.get_all_employees()
    watford = list(
        filter(
            lambda e: e.first_name == "Gretchen" and e.last_name == "Watford",
            emp
        )
    )[0]
    assert watford.base_salary == 4000

def test_tomas_no_leader():
    rm = RelationsManager()
    emp = rm.get_all_employees()
    tomas = list(
        filter(
            lambda e: e.first_name == "Tomas" and e.last_name == "Andre",
            emp
        )
    )[0]
    assert rm.is_leader(tomas) == False
    assert rm.get_team_members(tomas) == None
    
def test_jude_not_in_db():
    rm = RelationsManager()
    emp = rm.get_all_employees()
    jude = list(
        filter(
            lambda e: e.first_name == "Jude" and e.last_name == "Overcash",
            emp
        )
    )
    assert len(jude) == 0
    
